<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses'=>'PagesController@getHome', 'as'=>'home.index']);

Route::get('/about', ['uses'=>'PagesController@getAbout', 'as'=>'about.index']);

Route::get('/visuals', ['uses'=>'PagesController@getVisuals', 'as'=>'visuals.index']);

Route::get('/samen-werken', ['uses'=>'PagesController@getSamenwerken', 'as'=>'samenwerken.index']);
Route::post('/samen-werken', ['uses'=>'PagesController@postSamenwerken', 'as'=>'samenwerken.post']);

Route::get('/bedankt', ['uses'=>'PagesController@getBedankt', 'as'=>'bedankt.index']);
