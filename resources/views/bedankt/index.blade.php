@extends('layouts.front-end')

@section('fb_pixel')
@include('includes.fb_pixel_lead')
@stop

@section('content')

	@include('includes.header.logo')
	<section id="bedankt">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="ty-message">
						<h1>Je aanvraag is verstuurd!</h1>
					</div>
				</div>
			</div>
		</div>
	</section>

@stop