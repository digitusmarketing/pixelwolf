<!DOCTYPE html>

<html lang="nl-NL">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Pixelwolf</title>
	<link rel="icon" href="{!! url('favicon.ico') !!}" type="image/ico" />
	<link rel="shortcut icon" href="{!! url('favicon.ico') !!}" type="image/ico" />
	<link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
	<link rel="stylesheet" href="{!! url('css/bootstrap.min.css') !!}">
	<link rel="stylesheet" href="{!! url('css/bootstrap-theme.min.css') !!}">
	<link rel="stylesheet" href="{!! url('css/reset.css') !!}">
	<link rel="stylesheet" href="{!! url('css/main.css') !!}">
	<link rel="stylesheet" href="{!! url('css/font-awesome.css') !!}">

	@include('includes.fb_pixel')
	@yield('fb_pixel')
</head>
<body>
	@include('includes.header.mobile')
	@yield('content')

	@include('includes.footer')

	@yield('footerscript')

</body>
</html>