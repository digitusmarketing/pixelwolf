@extends('layouts.front-end')

@section('content')

	@include('includes.header.navbar')
	<section id="intro" class="samenwerken">
		<div class="intro overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Let's work together</h1>
				</div>
			</div>
		</div>
	</section>

	<section id="samenwerken">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					{{-- <h2>Lets work together</h2> --}}
					<h3>Ik hoor graag meer over jou en je projecten!</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3 class="company-name">Pixelwolf</h3>
					<h5 class="company-contact">
						<span class="tekst">06 83970714</span>
						<span class="devider"></span>
						<span class="tekst">contact@pixelwolf.nl</span>
					</h5>
					<h4 class="company-address">Hengelo, Overijssel<br/><span class="uppercase">the Netherlands</span></h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					{!! Form::open(array('class'=>'form-horizontal')) !!}
						<div class="form-group row">
							<div class="col-md-6">
								{!! Form::label('voornaam', 'Voornaam') !!}
								{!! Form::text('voornaam', old('voornaam'), array('class'=>'form-control')) !!}
							</div>
							<div class="col-md-6">
								{!! Form::label('achternaam', 'Achternaam') !!}
								{!! Form::text('achternaam', old('achternaam'), array('class'=>'form-control')) !!}
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								{!! Form::label('email', 'Email Adres') !!}
								{!! Form::email('email', old('email'), array('class'=>'form-control')) !!}
							</div>
							<div class="col-md-6">
								{!! Form::label('telefoonnummer', 'Telefoonnummer') !!}
								{!! Form::tel('telefoonnummer', old('telefoonnummer'), array('class'=>'form-control')) !!}
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								{!! Form::label('onderwerp', 'Onderwerp') !!}
								{!! Form::text('onderwerp', old('onderwerp'), array('class'=>'form-control')) !!}
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								{!! Form::label('bericht', 'Bericht') !!}
								{!! Form::textarea('bericht', old('bericht'), array('class'=>'form-control')) !!}
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								{!! Form::submit('Verzenden', array('class'=>'btn btn-work-black')) !!}
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</section>

@stop

@section('footerscript')
	<script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.inview.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
@stop