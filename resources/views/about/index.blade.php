@extends('layouts.front-end')

@section('content')

	@include('includes.header.navbar')
	<section id="about">
		<div class="intro overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div>
						<h1>Tim Helthuis</h1>
						<h2>The man behind the lens</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Tim Helthuis - Videographer</h2>
					<p>
						Ik ben een “run en gun” videograaf gespecialiseerd in Visual Vibes/Sfeer Impressies. Ik maak gebruik van hoge kwaliteit camera aparatuur en voornamelijk natuurlijk licht en omgeving. Geen grote productie sets zodat ik de vibe en spirit in zijn puurst en meest inspirationele vorm kan gebruiken. Werken met klant en omgeving is zo heel vrij. Reizen? Geen probleem. Persoonlijk contact? Geen probleem. Perfectie? Standaard.
					</p>
					{{-- <div class="row">
						<div class="col-md-6 col-md-offset-3">
							<img src="{!! url('/images/bg/About_extra 1.jpg') !!}" alt="Behind the scenes" style="margin-top:25px;margin-bottom:-50px;">
						</div>
					</div> --}}
				</div>
			</div>
		</div>
	</section>
	@include('includes.samenwerken')

@stop

@section('footer')
	<footer id="footer" class="home">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
				</div>
			</div>
		</div>
	</footer>
@stop

@section('footerscript')
	<script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.inview.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script>
		$(document).ready(function(){
			$('#about h4').on('inview',function(event, isInView){
				$('.floater-header').addClass('toggled');
			});
			$('#intro').on('inview', function(event, isInView){
				$('.floater-header').removeClass('toggled');
			});
		});
	</script>
@stop