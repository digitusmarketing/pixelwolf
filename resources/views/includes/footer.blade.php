<footer id="social">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 social-footer-holder">
				<ul class="nav navbar-nav" style="float:none;">
					<li>
						<a href="https://www.facebook.com/PixelwolfFilms/" target="_blank">
							<i class="fa fa-facebook"></i>
						</a>
					</li>
					<li>
						<a href="https://vimeo.com/timpixelwolf" target="_blank">
							<i class="fa fa-vimeo"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<footer id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<a href="mailto:tim@pixelwolf.nl">tim@pixelwolf.nl</a>
			</div>
		</div>
	</div>
</footer>