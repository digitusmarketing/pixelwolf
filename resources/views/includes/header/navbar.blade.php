<header id="header">
	<div class="intro-header">
		<div class="col-md-4">
			<div class="logo">
				<img src="{!! url('images/pixelwolf_logo_icon_white.png') !!}">
			</div>
		</div>
		<div class="col-md-8">
			<nav>
				<ul class="nav nav-pixel">
					<li class="nav-home"><a href="{!! route('home.index') !!}">Home</a></li>
					<li class="nav-about"><a href="{!! route('about.index') !!}">About</a></li>
					<li class="nav-visuals"><a href="{!! route('visuals.index') !!}">Visuals</a></li>
				</ul>
				<a class="btn btn-work" href="{!! route('samenwerken.index') !!}">Samen werken</a>
			</nav>
		</div>
	</div>
	<div class="floater-header">
		<div class="floater-flex">
			<div class="col-md-4">
				<div class="logo">
					<img src="{!! url('images/pixelwolf_logo_icon_white.png') !!}">
				</div>
			</div>
			<div class="col-md-8">
				<nav>
					<ul class="nav nav-pixel">
						<li class="nav-home"><a href="{!! route('home.index') !!}">Home</a><span class="line"></span></li>
						<li class="nav-about"><a href="{!! route('about.index') !!}">About</a><span class="line"></span></li>
						<li class="nav-visuals"><a href="{!! route('visuals.index') !!}">Visuals</a><span class="line"></span></li>
					</ul>
					<a class="btn btn-work" href="{!! route('samenwerken.index') !!}">Samen werken</a>
				</nav>
			</div>
		</div>
	</div>
</header>