<header id="header" class="bedankt">
	<div class="intro-header">
		<div class="col-md-4 col-md-offset-4">
			<div class="logo">
				<a href="{!! url('/') !!}">
					<img src="{!! url('images/pixelwolf_logo_website1.png') !!}">
				</a>
			</div>
		</div>
	</div>
</header>