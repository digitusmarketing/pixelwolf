{{-- <header id="mobile">
	<div class="intro-header">
		<div class="col-sm-6 col-xs-6">
			<div class="logo">
				<a href="{!! url('/') !!}">
					<img src="{!! url('images/pixelwolf_logo_icon_white.png') !!}">
				</a>
			</div>
		</div>
		<div class="col-sm-6 col-xs-6">
			<div class="bars-holder">
				<i class="fa fa-bars" id="mobile-menu"></i>
			</div>

		</div>
	</div>
</header> --}}
<header id="header" class="mobile">
	<div class="intro-header">
		<div class="col-sm-12 col-xs-12">
			<div class="logo" style="width: 80px;display: block;margin: 0 auto;">
				<a href="{!! url('/') !!}">
					<img src="{!! url('images/pixelwolf_logo_icon_white.png') !!}" style="display:block;margin:0 auto;float:none">
				</a>
			</div>
		</div>
	</div>
</header>
<section id="menu-toggler">
	<div class="bars-holder">
		<i class="fa fa-bars" id="mobile-menu"></i>
	</div>
</section>
<section id="mobile-menu">
	<section class="top">
		<div class="pull-right">
			<i class="fa fa-times" id="close-mobile-menu"></i>
		</div>
	</section>
	<section class="middle">
		<ul class="nav nav-mobile">
			<ul>
				<li class="nav-home"><a href="{!! route('home.index') !!}">Home</a></li>
				<li class="nav-about"><a href="{!! route('about.index') !!}">About</a></li>
				<li class="nav-visuals"><a href="{!! route('visuals.index') !!}">Visuals</a></li>
			</ul>
		</ul>
		<div class="samen-werken" style="margin-top:25px;">
			<a href="{!! route('samenwerken.index') !!}" class="btn-work-black btn">Samen werken</a>
		</div>
	</section>
	{{-- <section class="bottom">
		<div class="social-footer-holder">
			<ul class="nav navbar-nav" style="float:none;">
				<li>
					<a href="https://www.facebook.com/PixelwolfFilms/" target="_blank">
						<i class="fa fa-facebook"></i>
					</a>
				</li>
				<li>
					<a href="https://vimeo.com/timpixelwolf" target="_blank">
						<i class="fa fa-vimeo"></i>
					</a>
				</li>
			</ul>
		</div>
	</section> --}}
</section>