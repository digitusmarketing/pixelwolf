<!DOCTYPE html>

<html lang="nl-NL">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
</head>
<body>
	<div class="rcmBody" style="border:0; margin:0; padding:0; min-width:100%;">

		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td bgcolor="f9fafa" style="border:0; margin:0; padding:0;">
					<table style="background-color: #1d71b8" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td align="center" style="border: 0; margin: 0; padding: 0">
								<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
									<tbody>
										<tr>
											<td height="7" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
												<div class="clear" style="height: 7px; width: 1px">&nbsp;</div>
											</td>
										</tr>
										<tr>
											<td class="banner" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tbody><tr>
														<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
															<div class="clear" style="height: 1px; width: 20px"></div>
														</td>
														<td style="border: 0; margin: 0; padding: 0" width="100%">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tbody><tr>
																	<td align="center" class="icon" height="72" style="border: 0; margin: 0; padding: 0">
																		<a href="https://www.socialpreneurs.io" style="border: 0; margin: 0; padding: 0" rel="noreferrer" target="_blank">
																			<span class="retina">
																				<img height="72" src="https://www.socialpreneurs.io/images/site/logo-white.png" style="border: 0; margin: 0; padding: 0" width="129">
																			</span>
																		</a>
																	</td>
																</tr>
																<tr>
																	<td height="22" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<div class="clear" style="height: 22px; width: 1px">&nbsp;</div>
																	</td>
																</tr>
																<tr>
																	<td align="center" class="title" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px; text-shadow: 0 1px 1px #1b1d20">
																		<span class="apple-override" style="color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px">Onderwerp: {!! $onderwerp !!} </span>
																	</td>
																</tr>


																<tr>
																	<td height="13" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<div class="clear" style="height: 13px; width: 1px">&nbsp;</div>
																	</td>
																</tr>
																<tr>
																	<td align="center" height="1" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<table align="center" border="0" cellpadding="0" cellspacing="0" width="200">
																			<tbody>
																				<tr>
																					<td bgcolor="#1b1d20" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																						<div class="clear" style="height: 1px; width: 200px">&nbsp;</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
															</tbody></table>
														</td>
														<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
															<div class="clear" style="height: 1px; width: 20px"></div>
														</td>
													</tr>
												</tbody></table>
											</td>
										</tr>
										<tr>
											<td height="27" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
												<div class="clear" style="height: 27px; width: 1px">&nbsp;</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td align="center" style="border: 0; margin: 0; padding: 0">
									<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
										<tbody>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-large" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21px">

												{!! $voornaam !!} {!! $achternaam !!} heeft contact opgenomen.
												</td>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-large" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21px">

												Telefoonnummer: {!! $telefoonnummer !!}
												</td>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-large" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21px">

												Emailadres: {!! $email !!}
												</td>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-large" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21px">

												{{ $bericht }}.
												</td>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td colspan="3" align="center" height="1" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="200">
														<tbody>
															<tr>
																<td bgcolor="edeff0" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																	<div class="clear" style="height: 1px; width: 200px">&nbsp;</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
	</div>

</body>
</html>