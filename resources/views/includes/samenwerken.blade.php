<section id="samenwerken">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="flex-box">
					<h4>Ik hoor graag meer over jou en je projecten!</h4>
					<a href="{!! route('samenwerken.index') !!}" class="btn btn-work-black">Werk met mij</a>
				</div>
			</div>
		</div>
	</div>
</section>