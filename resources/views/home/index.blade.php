@extends('layouts.front-end')

@section('content')

	@include('includes.header.navbar')
	<section id="intro">
		{{-- <div class="intro overlay"></div> --}}
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div>
						
					<h2 class="sub-title">Pixelwolf</h2>
					<h1>Professional Video Solutions</h1>
					</div>
					<a class="btn btn-work" href="{!! route('samenwerken.index') !!}">Samen werken</a>
				</div>
			</div>
		</div>

		<div class="video-overlay" style="position:relative;">
			<div class="overlay"></div>
			{{-- <iframe src="https://player.vimeo.com/video/217401790?autoplay=1" frameborder="0"></iframe> --}}
			<video autoplay loop muted>
				<source src="https://player.vimeo.com/external/210136156.hd.mp4?s=cdb56b958509f4129b41f187503feb7341b39325&profile_id=119" type="video/mp4">
			</video>
		</div>
	</section>
	<section id="home">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>"If it can be writen or thought, it can be filmed."</h2>
					{{-- <h4>It's what I strive for in everything I photograph, film &amp; produce. </h4> --}}
				</div>
				<div class="clearfix"></div>
				<div class="home">
					<div class="col-md-4">
						<img src="{!! url('/images/bg/Style.jpg') !!}" alt="Style">
						<h3>Style</h3>
						<p>Geen sets, geen gedoe. De “run en gun” style waarmee ik Werk zorgt voor een betrokken, creatieve en effectieve stijl waarin tijd en ruimte is voor elke situatie.</p>
					</div>
					<div class="col-md-4">
						<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
						<h3>International</h3>
						<p>Of je nou retreats organiseerd op Ibiza, workshops geeft op Cuba of een klein café runt in het noorden van Scandinavie. Ik ga waar het verhaal verteld moet worden.</p>
					</div>
					<div class="col-md-4">
						<img src="{!! url('/images/bg/Collaboration.jpg') !!}" alt="Collaboration">
						<h3>Collaboration</h3>
						<p>Samenwerken of volledig uitbesteden. Ik sta open voor elke vorm van samenwerking.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="about">
		<div class="about overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div>
						<h1>Tim Helthuis</h1>
						<h2>The man behind the lens</h2>
					</div>
					<h4>Een “run en gun” videograaf gespecialiseerd in Visual Vibes/Sfeer Impressies.<br/>Ik maak gebruik van hoge kwaliteit camera aparatuur en voornamelijk natuurlijk licht en omgeving.<br/>Geen grote productie sets zodat ik de vibe en spirit in zijn puurste en meest inspirationele vorm kan gebruiken.<br/>Werken met klant en omgeving is zo heel vrij.
					</h4>
					<h3>Reizen?<br/>Geen probleem.<br/><br/>Persoonlijk contact?<br/>Geen probleem.<br/><br/>Perfectie?<br/>Standaard.</h3>
				</div>
			</div>
		</div>
	</section>
	<section id="visuals">
		{{-- <div class="container"> --}}
		<div class="col-md-12">
			
			<div class="row">
				<div class="col-md-12">
					<h2>Videography</h2>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-md-10 col-md-offset-1">
			<div class="row">
				
			{{-- <div class="row"> --}}
				<div class="col-md-12 video-box">
					<iframe src="https://player.vimeo.com/video/173772109?portrait=0&amp;byline=0&amp;title=0&amp;playbar=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
			</div>
		</div>
			{{-- </div> --}}
			<div class="clearfix"></div>
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					
			{{-- <div class="row"> --}}
					<div class="col-md-6 video-box">
						<iframe src="https://player.vimeo.com/video/190707021?portrait=0&amp;byline=0&amp;title=0&amp;playbar=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<div class="col-md-6 video-box">
						<iframe src="https://player.vimeo.com/video/166353407?portrait=0&amp;byline=0&amp;title=0&amp;playbar=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
			{{-- </div> --}}
			{{-- <div class="row" style="margin:15px 0;">
				<div class="col-md-6">
					<div class="row" style="padding:0 0 15px;">
						<div class="col-md-6">
							<img src="{!! url('/images/bg/Style.jpg') !!}" alt="International">
						</div>
						<div class="col-md-6">
							<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
						</div>
					</div>
					<div class="row" style="padding:15px 0 0;">
						<div class="col-md-6">
							<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
						</div>
						<div class="col-md-6">
							<img src="{!! url('/images/bg/Style.jpg') !!}" alt="International">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
				</div>
			</div>
			<div class="row" style="padding:15px ;">
				<div class="col-md-4">
					<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
				</div>
				<div class="col-md-4">
					<img src="{!! url('/images/bg/Style.jpg') !!}" alt="International">
				</div>
				<div class="col-md-4">
					<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
				</div>
			</div>
			<div class="row" style="margin:15px 0;">
				<div class="col-md-6">
					<img src="{!! url('/images/bg/Style.jpg') !!}" alt="International">
				</div>
				<div class="col-md-6">
					<div class="row" style="padding:0 0 15px">
						<div class="col-md-6">
							<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
						</div>
						<div class="col-md-6">
							<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
						</div>
					</div>
					<div class="row" style="padding: 15px 0 0">
						<div class="col-md-6">
							<img src="{!! url('/images/bg/International.jpg') !!}" alt="International">
						</div>
						<div class="col-md-6">
							<img src="{!! url('/images/bg/Style.jpg') !!}" alt="International">
						</div>
					</div>
				</div>
			</div> --}}
		{{-- </div> --}}
	</section>
	{{-- <section id="videography">
		<div class="videography-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>Videography</h2>
					</div>
				</div>
			</div>
		</div>
		
	</section> --}}
	@include('includes.samenwerken')

@stop

@section('footerscript')
	<script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.inview.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script>
		$(document).ready(function(){
			$('#about').on('inview',function(event, isInView){
				$('.floater-header').addClass('toggled');
			});
			$('#intro h2').on('inview', function(event, isInView){
				$('.floater-header').removeClass('toggled');
			});
		});
	</script>
@stop