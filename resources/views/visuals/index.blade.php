@extends('layouts.front-end')

@section('content')

	@include('includes.header.navbar-inverse')
	<section id="visuals" class="page">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Videography</h2>
				</div>
			</div>
		</div>
		<div class="col-md-10 col-md-offset-1">
			<div class="row">
				<div class="col-md-12 video-box">
					<iframe src="https://player.vimeo.com/video/173772109?portrait=0&amp;byline=0&amp;title=0&amp;playbar=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-6 video-box">
					<iframe src="https://player.vimeo.com/video/190707021?portrait=0&amp;byline=0&amp;title=0&amp;playbar=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<div class="col-md-6 video-box">
					<iframe src="https://player.vimeo.com/video/166353407?portrait=0&amp;byline=0&amp;title=0&amp;playbar=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	<section id="stills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Stills from other projects</h2>
				</div>
			</div>
			<div class="row" style="margin:15px 0;">
				<div class="col-md-6">
					<div class="row" style="padding:0 0 15px;">
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image:url('/images/visuals/visual_10.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_10.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_9.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_9.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
					</div>
					<div class="row" style="padding:15px 0 0;">
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_2.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_2.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_3.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_3.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6" style="min-height:330px;">
					<div class="visual-box" style="background-image: url('/images/visuals/visual_1.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
						{{-- <img src="{!! url('/images/visuals/visual_1.jpg') !!}" alt="International"> --}}
						<div class="overlay"></div>
					</div>
				</div>
			</div>
			{{-- <div class="row" style="padding:15px ;">
				<div class="col-md-4">
					<img src="{!! url('/images/visuals/visual_4.jpg') !!}" alt="International">
				</div>
				<div class="col-md-4">
					<img src="{!! url('/images/visuals/visual_5.jpg') !!}" alt="International">
				</div>
				<div class="col-md-4">
					<img src="{!! url('/images/visuals/visual_6.jpg') !!}" alt="International">
				</div>
			</div> --}}
			<div class="clearfix"></div>
			<div class="row" style="margin:15px 0;">
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-12" style="min-height:150px;margin:0 0 15px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_4.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_4.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
						<div class="col-md-12" style="min-height:150px; margin:15px 0 0;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_5.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_5.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6" style="min-height:330px;">
					<div class="visual-box" style="background-image: url('/images/visuals/visual_7.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
						{{-- <img src="{!! url('/images/visuals/visual_7.jpg') !!}" alt="International"> --}}
						<div class="overlay"></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-12" style="min-height:150px;margin: 0 0 15px">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_6.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_6.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
						<div class="col-md-12" style="min-height:150px;margin:15px 0 0">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_8.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								{{-- <img src="{!! url('/images/visuals/visual_8.jpg') !!}" alt="International"> --}}
								<div class="overlay"></div>
							</div>
						</div>
					</div>
				</div>
				{{-- <div class="col-md-6">
					<div class="row" style="padding:0 0 15px">
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_4.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								<img src="{!! url('/images/visuals/visual_4.jpg') !!}" alt="International">
							</div>
						</div>
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_5.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								<img src="{!! url('/images/visuals/visual_5.jpg') !!}" alt="International">
							</div>
						</div>
					</div>
					<div class="row" style="padding: 15px 0 0">
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_6.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								<img src="{!! url('/images/visuals/visual_6.jpg') !!}" alt="International">
							</div>
						</div>
						<div class="col-md-6" style="min-height:150px;">
							<div class="visual-box" style="background-image: url('/images/visuals/visual_8.jpg'); background-size:cover;background-repeat:no-repeat;display:block;position:absolute;top:0;bottom:0;left:15px;right:15px;">
								<img src="{!! url('/images/visuals/visual_8.jpg') !!}" alt="International">
							</div>
						</div>
					</div>
				</div> --}}
			</div>
		</div>
	</section>
	@include('includes.samenwerken')

@stop

@section('footerscript')
<script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
@stop