<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;

class PagesController extends Controller {
	
	public function getHome()
	{
		return view('home.index');
	}

	public function getAbout()
	{
		return view('about.index');
	}

	public function getVisuals()
	{
		return view('visuals.index');
	}

	public function getSamenwerken()
	{
		return view('samenwerken.index');
	}

	public function postSamenwerken(Request $request)
	{
		$data = [
			'voornaam'	=> $request->voornaam,
			'achternaam'	=> $request->achternaam,
			'email'	=> $request->email,
			'telefoonnummer'	=> $request->telefoonnummer,
			'onderwerp'	=> $request->onderwerp,
			'bericht'	=> $request->bericht,
		];
		Mail::send('includes.email.samenwerken', $data, function($message) use($data)
			{
				// $message->from($data['email'], $data['voornaam']);
				$message->to('tim@pixelwolf.nl', 'Tim')->subject($data['onderwerp']);
			});

		return redirect()->route('bedankt.index');
	}

	public function getBedankt()
	{
		return view('bedankt.index');
	}

}