$(document).ready(function(){
	$('iframe').each(function(){
		$(this).css({'height':($(this).width() * 0.57)});
	});
	$(window).resize(function(){
		$('iframe').each(function(){
			$(this).css({'height':($(this).width() * 0.57)});
		});
	});

	$('i#mobile-menu').on('click', function(){
		$('section#mobile-menu').addClass('inscreen');
	});
	$('i#close-mobile-menu').on('click', function() {
		$('section#mobile-menu').removeClass('inscreen');
	});

});